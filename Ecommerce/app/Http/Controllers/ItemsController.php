<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ItemsController extends Controller
{
    // public function create(){
    // 	$categories = categories::all();

    // 	return view('adminviews.add-item', compact('categories'));
    // }

	#User functions
	#index
	public function index(){
		$item = items::all();

		return view('catalog')->with('items',$items);
	}

	#Admin functions

	public function adminCreate(){
		$categories = categories::all();

		return view('adminviews.additem', compact('categories'));
	}


    public function store(Request $req){
    	//validate
    	$rules = array(
    		"name" => "required",
    		"description" => "required",
    		"price" => "required|numeric",
    		"category_id" => "required",
    		"imgPath" => "required|image|mimes:jpeg,png,jpg,gif,tiff,tif,webp,bitmap|max:2048"
    	);

    	#accepts 2 param
    	$this->validate($req,$rules);

    }
}
