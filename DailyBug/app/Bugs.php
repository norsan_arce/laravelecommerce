<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Bugs extends Model
{
    public function category(){
    	return $this->belongsTo("\App\Categories");
    }

    public function status(){
    	return $this->belongsTo('\App\Statuses');
    }

    public function user(){
    	return $this->belongsTo('\App\User');
    }
}
