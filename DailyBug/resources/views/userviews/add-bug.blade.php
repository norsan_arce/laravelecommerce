@extends("layouts.app")
@section("content")

<h1 class="text-center py-5">Report A Bug</h1>

<article class="col-lg-6 offset-3">
	<form action="/addbugs" method="POST">
		@csrf
		<section class="form-group">
			<label for="title">Bug Title</label>
			<input type="text" name="title" class="form-control">
		</section>
		<section class="form-group">
			<label for="body">Bug Description</label>
			<input type="text" name="body" class="form-control">
		</section>
		<section class="form-group">
			<label for="category">Category</label>
			<select name="category_id" class="form-control">
				@foreach($categories as $indiv_category);
				<option value="{{$indiv_category->id}}">{{$indiv_category->name}}
				</option>
				@endforeach
			</select>
		</section>
		<button class="btn btn-success" type="submit">Submit Report</button>
	</form>
</article>
@endsection