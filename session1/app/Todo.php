<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use \App\Category;
use \App\Status;

class Todo extends Model
{
	public function Category(){
		return $this->belongsTo("\App\Category");
	}

	public function Status(){
		return $this->belongsTo("\App\Status");
	}
}
