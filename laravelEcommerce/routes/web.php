<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/operators', 'ItemController@index');

Route::get('/add-operator', 'ItemController@create');

Route::post('/add-operator', 'ItemController@store');

#Delete Item
Route::delete('/delete-operator', 'ItemController@deleteitem');

#Edit Item
Route::patch('edititem/{id}','ItemController@update');


#Cart CRUD
Route::post('/addtocart/{id}', 'ItemController@addToCart');

Route::get('/showcart', 'ItemController@showCart');