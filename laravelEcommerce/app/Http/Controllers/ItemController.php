<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use \App\Item;
use \App\Category;
use Session;

class ItemController extends Controller
{
    //catalog index
    public function index(){

    	$items = Item::all();

    	return view('catalog', compact('items'));
    }

    //add an item form
    public function create(){

    	$categories = Category::all();

    	return view ('adminviews.additem', compact('categories'));
    }

    //add an item to the database
    public function store(Request $req){
    	
    	//form validation
    	$rules = array(
    		"name" => "required",
    		"description" => "required",
    		"price" => "required|numeric",
    		"category_id" => "required",
    		"image" => "required|image|mimes:jpeg, jpg, png, gif, tiff, tif, webp"
    	);
    	
    	$this->validate($req, $rules);


        $item->name = $req->name;
        $item->description = $req->description;
        $item->price = $req->price;
        $item->category_id = $req->category_id;

        if($req->file('imgPath') != null){
            $image = $req->file('imgPath');
            $image_name = time(). ".". $image->getClientOriginalExtension();
            $destination = "images/";
            $image->move($destination, $image_name);
            $item->imgPath = $destination.$image_name;
        }

        $item->save();
        Session::flash('message', "$item->name has been updated.");
        return redirect('/catalog');
    	
    	//form capture
    	$newOperator = new Item;
    	
    	$newOperator->name = $req->name;
    	$newOperator->description = $req->description;
    	$newOperator->price = $req->price;
    	$newOperator->category_id = $req->category_id;

    	//image handling
    	$image = $req->file('image');
    	$image_name = time().".".$image->getClientOriginalExtension();

    	$destination = "images/";

    	$image->move($destination, $image_name);

    	$newOperator->imgPath = $destination.$image_name;

    	//save the new item to the database
    	$newOperator->save();

    	Session::flash("message", "$newOperator->name has been addded!");

    	//return to catalog
    	return redirect('/operators');

    } public function deleteitem($id){
        $toDelete = Item::find($id);
        $ToDelete->Delete();

        return redirect()->back();
    }

    public function addToCart($id, Request $req){
        # check if there is an existing session
        if(Session::has('cart')){

            $cart = Session::get('cart');
        }else {
            $cart = [];
        }
        # check if this is the first time we'll add an item to our cart 

        if(isset($cart[$id])){
            $cart[$id] += $req->quantity;
        } else {
            $cart[$id] = $req->quantity;
        }

        Session::put("cart", $cart);

        $item = Item::find($id);

        Session::flash('message',"$req->quantity of $item->name successfully added to cart");

        #Flash a message
        #Find the item
        #Use the name and quantity in your flash message
        #Ex: 2 of item name successfully added to cart

        return redict()->back();

    }

    public function showCart(){

        #We will create a new array containing item name, price , quantity and subtotal
        # initialize an empty array

        $items = [];
        $total = 0;

        if(Session::has('cart')){
            $cart = Session::get('cart');
            foreach($cart as $itemId => $quantity){
                $item = Item::find($itemId);
                $item->quantity = $quantity;
                $item->subtotal = $item->price * $quantity;
                $items[] = $item;
                $total += $item->subtotal;
            }
        }

        return view('userviews.cart', compact('items','total'));
    }
}
